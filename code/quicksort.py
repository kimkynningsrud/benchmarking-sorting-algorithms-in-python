import sys
sys.setrecursionlimit(2000)

def partition(A, p, r):
    x = A[r]
    i = p - 1
    for j in range(p, r - 1):
        if A[j] <= x:
            i+= 1
            A[i], A[j]= A[j], A[i]
    A[i + 1], A[r] = A[r], A[i + 1]
    return i + 1


def runquicksort(A,p,r):
    if p<r:
        q = partition(A,p,r)
        runquicksort(A,p,q-1)
        runquicksort(A,q+1,r)

def quicksort(A):
    runquicksort(A, 0, len(A)-1)
