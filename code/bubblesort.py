def bubble(A):
    N = len(A)
    for j in range(N - 1):
        for i in range(N - 1):
            if A[i] > A[i+1]:
                A[i], A[i + 1] = A[i + 1], A[i]