m = 32

def quicksort(A, low, high):
    if low < high:
        sizeArr = high - low + 1
        if (sizeArr < m):
            insertion_sort(A, low, high)
        else:
            mid = partition(A, low, high)
            quicksort(A, low, mid - 1)
            quicksort(A, mid + 1, high)


def partition(A, low, high):
    piv = A[high]
    i = low - 1
    for j in range(low, high):
        if A[j] < piv:
            i += 1
            temp = A[i]
            A[i] = A[j]
            A[j] = temp

    tempo = A[i + 1]
    A[i + 1] = A[high]
    A[high] = tempo

    return i + 1


def insertion_sort(A, first, last):
    for i in range(first + 1, last + 1):
        key = A[i]
        j = i - 1
        while key < A[j] and j >= first:
            A[j + 1] = A[j]
            j -= 1
        A[j + 1] = key
    return A



def quick_insertion_combine(numList):
    quicksort(numList, 0, len(numList) - 1)