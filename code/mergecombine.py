import math



def find_minrun(n):
    MINIMUM = 32
    r = 0
    while n >= MINIMUM:
        r |= n & 1
        n >>= 1
    return n + r


def insertion_sort(A, left, right):
    for i in range(left + 1, right + 1):
        key = A[i]
        j = i - 1
        while key < A[j] and j >= left:
            A[j + 1] = A[j]
            j -= 1
        A[j + 1] = key
    return A


def merge(arr, l, m, r):
    nL = m - l + 1
    nR = r - m

    L = [0] * (nL + 1)
    R = [0] * (nR + 1)

    for i in range(0, nL):
        L[i] = arr[l + i]

    for j in range(0, nR):
        R[j] = arr[m + 1 + j]

    L[nL] = math.inf
    R[nR] = math.inf
    i = 0
    j = 0
    for k in range(l, r + 1):
        if L[i] <= R[j]:
            arr[k] = L[i]
            i += 1
        else:
            arr[k] = R[j]
            j += 1

def merge_insertion_combine(array):
    n = len(array)
    minrun = find_minrun(n)

    for start in range(0, n, minrun):
        end = min(start + minrun - 1, n - 1)
        insertion_sort(array, start, end)

    size = minrun
    while size < n:

        for left in range(0, n, 2 * size):
            mid = min(n - 1, left + size - 1)
            right = min((left + 2 * size - 1), (n - 1))
            merge(array, left, mid, right)

        size = 2 * size
